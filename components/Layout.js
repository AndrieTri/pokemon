import Head from 'next/head'

const Home = ({ children, title }) => {
    return (
        <div 
            style={{ 
                backgroundImage: "url(" + "/backgroundpokeball.png" + ")",
                backgroundPosition: 'center',
                backgroundSize: 'cover',
                backgroundRepeat: 'no-repeat'
            }}
        >
            <Head>
                <title>{title}</title>
                <link rel="icon" href="/pokeball.png" />
            </Head>

            <main className="container mx-auto max-w-xl pt-8 min-h-screen">
                {children}
            </main>
        </div>
    )
}

export default Home
