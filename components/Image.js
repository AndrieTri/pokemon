const Image = (props) => {
    return (
        <>
            <img
                src={props.image}
                alt={props.name}
                className="w-20 h-20 mr-3"
            />
        </>
    )
}

export default Image
