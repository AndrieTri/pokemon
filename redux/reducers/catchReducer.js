import {
    ADD_DATA,
    REMOVE_DATA
} from '../../utils/actionTypes'

const initialReducer = {
    data: []
}

const catchReducer = (state = initialReducer, action) => {
    switch (action.type) {
        case ADD_DATA:
            return {
                data: [...state.data, action]
            }
        case REMOVE_DATA:
            return {
                data: state.data.filter(datasa => datasa.item.nickname !== action.item)
            }
        case "persist/REHYDRATE": 
            if (action && action.payload) {
                const {
                    payload: { catchReducer }
                } = action

                return { 
                    data: catchReducer.data
                }
            }

            return { ...state }
        default:
            return { ...state }
    }
}

export default catchReducer