import pokemonReducer from './pokemonReducer'
import pokemonDetailReducer from './pokemonDetailReducer'
import catchReducer from './catchReducer'
import { combineReducers } from 'redux'

const rootReducer = combineReducers({
    pokemon: pokemonReducer,
    pokemonDetail: pokemonDetailReducer,
    catchReducer: catchReducer
});

export default rootReducer