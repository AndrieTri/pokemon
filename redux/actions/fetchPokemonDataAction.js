import axios from 'axios'
import { apiBaseURL } from '../../utils/constants'
import {
    FETCHING_POKEMON_DATA,
    FETCHING_POKEMON_DATA_SUCCESS,
    FETCHING_POKEMON_DATA_FAIL
} from '../../utils/actionTypes'

const fetchPokemonDataAction = () => {
    const axiosConfig = {
        headers: {}
    };

    return dispatch => {
        dispatch({ type: FETCHING_POKEMON_DATA })

        return axios.get(`${apiBaseURL}/api/v2/pokemon?limit=15`, axiosConfig)
            .then((res) => {
                return dispatch({ type: FETCHING_POKEMON_DATA_SUCCESS, payload: res.data });
            })
            .catch((err) => {
                return dispatch({ type: FETCHING_POKEMON_DATA_FAIL, payload: err.data });
            })
    }
}

export default fetchPokemonDataAction


