import axios from 'axios'
import { apiBaseURL } from '../../utils/constants'
import {
    FETCHING_POKEMON_DETAIL_DATA,
    FETCHING_POKEMON_DETAIL_DATA_SUCCESS,
    FETCHING_POKEMON_DETAIL_DATA_FAIL
} from '../../utils/actionTypes'

const fetchPokemonDataAction = ({ id }) => {
    const axiosConfig = {
        headers: {}
    };

    return dispatch => {
        dispatch({ type: FETCHING_POKEMON_DETAIL_DATA })

        return axios.get(`${apiBaseURL}/api/v2/pokemon/${id}`, axiosConfig)
            .then((res) => {
                return dispatch({ type: FETCHING_POKEMON_DETAIL_DATA_SUCCESS, payload: res.data });
            })
            .catch((err) => {
                return dispatch({ type: FETCHING_POKEMON_DETAIL_DATA_FAIL, payload: err.data });
            })
    }
}

export default fetchPokemonDataAction


