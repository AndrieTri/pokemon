import {
    ADD_DATA,
    REMOVE_DATA
} from '../../utils/actionTypes'

export const addItems = item => ({
    type: ADD_DATA,
    item
});

export const removeItems = item => ({
    type: REMOVE_DATA,
    item
});