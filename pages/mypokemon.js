import Layout from '../components/Layout'
import { useSelector } from 'react-redux'
import Link from 'next/link'
import Image from '../components/Image'
import { removeItems } from '../redux/actions/catchAction'
import { useDispatch } from 'react-redux'

const App = ({ }) => {
    let content
    const pokemon = useSelector(state => state.catchReducer.data)
    const dispatch = useDispatch()

    if (pokemon) {
        content = (
            <>
                {pokemon.map((pokeman, index) => (
                    <li key={index}>
                            <div className="grid grid-cols-4 border p-4 border-grey my-2 hover:shadow-md capitalize flex items-center text-lg bg-red-400 rounded-md text-white">
                                <div className="grid grid-cols-3 col-span-3">
                                    <div>
                                        <Image image={pokeman.item.image} name={pokeman.item.name} />
                                    </div>
                                    <div className="mt-6">
                                        <span className="mr-2 font-bold">
                                            {index + 1}.
                                        </span>
                                        {pokeman.item.nickname} - {pokeman.item.name}
                                    </div>
                                </div>

                                <div>
                                    <a className="text-white p-2 bg-red-600 hover:bg-red-700 rounded-md" onClick={() => dispatch(removeItems(pokeman.item.nickname))}>
                                        Remove!
                                    </a>
                                </div>
                            </div>
                    </li>
                ))}
            </>
        )
    }else{
        content = (
            <>
            </>
        )
    }

    return (
        <>
            <Layout title="My Pokemon">
                <h1 className="text-4xl mb-10 text-center text-white">My Pokemon</h1>
                <Link href={`/`}>
                    <a className="text-white p-2 bg-red-600 hover:bg-red-700 rounded-md">Pokemon List</a>
                </Link>
                <br />
                <br />
                <ul>
                    {content}
                </ul>
                <br />
            </Layout>
        </>
    )
}

export default App