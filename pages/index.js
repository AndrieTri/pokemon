import React, { useEffect, useState } from 'react'
import Link from 'next/link'
import { useDispatch, useSelector } from "react-redux"
import fetchPokemonDataAction from '../redux/actions/fetchPokemonDataAction'
import Layout from '../components/Layout'
import Image from '../components/Image'
import Loader from 'react-loader-spinner'

const useFetch = () => {
    const [data, setData] = useState(null)
    const [loading, setLoading] = useState(true)
    const dispatch = useDispatch()

    // Similar to componentDidMount and componentDidUpdate:
    useEffect(() => {
        const runEffect = async () => {
            const res = await dispatch(fetchPokemonDataAction())
            const { results } = await res.payload

            const item = results.map((pokeman, index) => {
                const paddedId = ('00' + (index + 1)).slice(-3)
                const image = `https://assets.pokemon.com/assets/cms2/img/pokedex/detail/${paddedId}.png`

                return { ...pokeman, image }
            });

            setData(item)
            setLoading(false)
        };
        runEffect()
    }, [])

    return {
        data,
        loading
    };
}

const App = ({ }) => {
    let content
    const pokemon = useSelector((state) => state.pokemon)
    const { data, loading } = useFetch()
    const pokemonCatch = useSelector(state => state.catchReducer.data)

    if (pokemon.process === 'DONE' && !loading){
        content = (
            <>
                {data.map((pokeman, index) => 
                    (
                        <li key={index}>
                            <Link href={`/pokemon?id=${index + 1}`}>
                                <a className="grid grid-cols-4 border p-4 border-grey my-2 hover:shadow-md capitalize flex items-center text-lg bg-red-400 rounded-md text-white">
                                    <div className="grid grid-cols-3 col-span-3">
                                        <div>
                                            <Image image={pokeman.image} name={pokeman.name} />
                                        </div>
                                        <div className="mt-6">
                                            <span className="mr-2 font-bold">
                                                {index + 1}.
                                            </span>
                                            {pokeman.name}
                                            <span className="mx-2 font-bold">
                                                ({pokemonCatch ? pokemonCatch.filter(datasa => datasa.item.name === pokeman.name).length : '0' })
                                            </span>
                                        </div>
                                    </div>
                                </a>
                            </Link>
                        </li>
                    )
                )}
            </>
        )
    }else{
        content = (
            <>
                <center>
                    <Loader
                        type="Audio"
                        color="#e74c3c"
                        height={100}
                        width={100}
                        timeout={10000} //3 secs
                    />
                </center>
            </>
        )
    }

    return (
        <>
            <Layout title="Pokemon">
                <h1 className="text-4xl mb-10 text-center text-white">Pokemon List</h1>
                <Link href={`/mypokemon`}>
                    <a className="text-white p-2 bg-red-600 hover:bg-red-700 rounded-md">My Pokemon</a>
                </Link>
                <br />
                <br />
                <ul>
                    { content }
                </ul>
                <br />
            </Layout>
        </>
    )
}


App.getInitialProps = async ({ store }) => {
    return {}
}

export default App
