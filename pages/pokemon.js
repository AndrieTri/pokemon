import React, { useState } from 'react'
import Layout from '../components/Layout'
import Link from 'next/link'
import fetchPokemonDetailDataAction from '../redux/actions/fetchPokemonDetailDataAction'
import { addItems } from '../redux/actions/catchAction'
import { useDispatch, useSelector } from 'react-redux'
import Router from 'next/router'
import Modal from 'react-modal'

const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: '50%',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)'
    }
}

const catchPokemon = (setIsOpen, setIsOpenTwo) => {
    const number = Math.round(Math.random())

    if(number === 0){
        openModal(setIsOpenTwo)
    }else{
        openModal(setIsOpen)
    }
}

const onSubmit = (event, dispatch, results, setIsOpenTwo) => {
    event.preventDefault(event)
    results.nickname = event.target.name.value

    dispatch(addItems(results))
    Router.push('/mypokemon')

    closeModal(setIsOpenTwo)
}

const openModal = (setIsOpen) => {
    setIsOpen(true)
}

const closeModal = (setIsOpen) => {
    setIsOpen(false)
}

// Make sure to bind modal to your appElement (http://reactcommunity.org/react-modal/accessibility/)
Modal.setAppElement('div')

const pokemon = ({ props }) => {
    const dispatch = useDispatch()
    const dataCatch = useSelector(state => state.catch)
    const { results } = props
    const [modalIsOpen, setIsOpen] = useState(false)
    const [modalIsOpenTwo, setIsOpenTwo] = useState(false)
    const [name, setName] = useState()

    return (
        <>
            <Layout title={results.name}>
                <div className="border p-6 border-grey hover:shadow-md rounded-md bg-red-400">
                    <h1 className="text-4xl mb-2 text-center capitalize text-white">
                        {results.id}. {results.name}
                    </h1>
                    <img className="mx-auto" src={results.image} alt={results.name} />
                    <p className="text-white">
                        <span className="font-bold mr-2 text-white">Weight:</span> {results.weight}
                    </p>
                    <p className="text-white">
                        <span className="font-bold mr-2 text-white">Height:</span>
                        {results.height}
                    </p>
                    <h2 className="text-2xl mt-6 mb-2 text-white">Types</h2>
                    {results.types.map((type, index) => (
                        <p className="text-white" key={index}>{type.type.name}</p>
                    ))}

                    <h2 className="text-2xl mt-6 mb-2 text-white">Moves</h2>
                    {results.moves.slice(0,5).map((move, index) => (
                        <p className="text-white" key={index}>{move.move.name}</p>
                    ))}

                    <p className="mt-10 text-center">
                        <a className="text-white p-2 bg-red-600 hover:bg-red-700 rounded-md" onClick={() => catchPokemon(setIsOpen, setIsOpenTwo)}>
                            Catch Me!
                        </a>
                    </p>

                    <p className="mt-10 text-center">
                        <Link href="/">
                            <a className="text-2xl underline text-white">Pokemon List</a>
                        </Link>
                    </p>
                </div>

                <Modal
                    isOpen={modalIsOpen}
                    onRequestClose={() => closeModal(setIsOpen)}
                    style={customStyles}
                    appElement={document.getElementById('app')}
                >
                    <center>
                        <div className="md:w-1/3 sm:w-full rounded-lg bg-white">
                            <span className="font-bold text-lg text-red-700">Failed</span>

                            <div className="text-gray-600 mt-5 mb-10">
                                Please try catch again!
                            </div>

                            <button className="text-white p-2 bg-red-600 hover:bg-red-700 rounded-md" onClick={() => closeModal(setIsOpen)}>close</button>
                        </div>
                    </center>
                </Modal>

                <Modal
                    isOpen={modalIsOpenTwo}
                    onRequestClose={() => closeModal(setIsOpenTwo)}
                    style={customStyles}
                    appElement={document.getElementById('app')}
                >
                    <center>
                        <div className="mb-10">
                            <form onSubmit={(event) => onSubmit(event, dispatch, results, setIsOpenTwo)}>
                                <span className="font-bold text-lg text-red-700">Yeay Congrats!</span>
                                <br />
                                <input 
                                    placeholder="Input pokemon name" 
                                    className="p-2 focus:ring-2"
                                    required="required"
                                    id="name"
                                />
                                <button 
                                    className="text-white p-2 bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-blue-600 focus:ring-opacity-50"
                                    type="submit"
                                >
                                    Submit
                                </button>
                            </form>
                        </div>

                        <button className="text-white p-2 bg-red-600 hover:bg-red-700 rounded-md" onClick={() => closeModal(setIsOpenTwo)}>close</button>
                    </center>
                </Modal>
            </Layout>
        </>
    )
}

pokemon.getInitialProps = async ({ store, query }) => {
    const id = query.id
    try {
        const res = await store.dispatch(fetchPokemonDetailDataAction({id}))
        const results = await res.payload
        const paddedId = ('00' + id).slice(-3)
        results.image = `https://assets.pokemon.com/assets/cms2/img/pokedex/detail/${paddedId}.png`

        return {
            props: { results }
        };
    } catch (err) {
        console.error(err)
    }
}

export default pokemon