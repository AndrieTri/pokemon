import React, { useEffect, useState } from 'react'
import Link from 'next/link'
import { useDispatch, useSelector } from "react-redux"
import fetchPokemonDataAction from '../redux/actions/fetchPokemonDataAction'
import Layout from '../components/Layout'

const App = ({props}) => {
    // const pokemonData = useSelector(state => state.pokemon.data)
    const [pokemon] = useState(props.pokemonState);

    // useEffect(() => {
    //     // Update the document title using the browser API
    //     setPokemon(props.pokemonState.payload.results)
    // });

    const content = (
        <>
            {pokemon.map((pokeman, index) => (
                <li key={index}>
                    <Link href={`/pokemon?id=${index + 1}`}>
                        <a className="border p-4 border-grey my-2 hover:shadow-md capitalize flex items-center text-lg bg-red-400 rounded-md text-white">
                            <img
                                src={pokeman.image}
                                alt={pokeman.name}
                                className="w-20 h-20 mr-3"
                            />
                            <span className="mr-2 font-bold">
                                {index + 1}.
                                    </span>
                            {pokeman.name}
                        </a>
                    </Link>
                </li>
            ))}
        </>
    )

    return (
        <>
            <Layout title="Pokemon">
                <h1 className="text-4xl mb-10 text-center text-white">Pokemon List</h1>
                <ul>
                    { content }
                </ul>
                <br />
            </Layout>
        </>
    )
}

App.getInitialProps = async({store}) => {
    try {
        const res = await store.dispatch(fetchPokemonDataAction())

        const { results } = await res.payload

        const pokemonState = results.map((pokeman, index) => {
            const paddedId = ('00' + (index + 1)).slice(-3)

            const image = `https://assets.pokemon.com/assets/cms2/img/pokedex/detail/${paddedId}.png`

            return { ...pokeman, image }
        });

        return {
            props: { pokemonState }
        };
    } catch(err) {
        console.error(err)
    }
}

export default App